﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Subcategory : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [InverseProperty("Subcategories")]
        public virtual Category Category { get; set; }

        [InverseProperty("Subcategory")]
        public virtual ICollection<Good> Goods { get; set; }
    }
}
