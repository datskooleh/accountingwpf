﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Good : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public decimal PriceBuy { get; set; }

        public decimal PriceSell { get; set; }

        public int Warranty { get; set; }

        [InverseProperty("Goods")]
        public virtual Subcategory Subcategory { get; set; }
    }
}
