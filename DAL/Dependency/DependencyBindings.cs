﻿using DAL.DTO;
using DAL.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Dependency
{
    public class DependencyBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ICategoryRepository>().To<CategoryRepository>();
            Bind<ISubcategoryRepository>().To<SubcategoryRepository>();
            Bind<IGoodRepository>().To<GoodRepository>();
        }
    }
}
