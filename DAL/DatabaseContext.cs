﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DatabaseContext : DbContext
    {
        private static readonly DatabaseContext instance = new DatabaseContext();
        public static DatabaseContext Instance { get { return instance; } }

        public DatabaseContext()
            : base("Accounting")
        {

        }

        public DatabaseContext(string connectionString)
            : base(connectionString) { }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Subcategory> Subcategories { get; set; }

        public DbSet<Good> Goods { get; set; }
    }
}
