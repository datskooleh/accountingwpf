﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    public class SubcategoryDTO : IDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CategoryDTO Category { get; set; }

        public ICollection<GoodDTO> Goods { get; set; }
    }
}
