﻿using DAL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class GoodDTO : IDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public decimal PriceBuy { get; set; }

        public decimal PriceSell { get; set; }

        public int Warranty { get; set; }

        public SubcategoryDTO Subcategory { get; set; }
    }
}
