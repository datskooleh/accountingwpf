﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DTO
{
    public class CategoryDTO : IDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<SubcategoryDTO> Subcategories { get; set; }
    }
}
