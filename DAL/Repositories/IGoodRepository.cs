﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public interface IGoodRepository : IRepository<Good, GoodDTO>
    {
    }
}
