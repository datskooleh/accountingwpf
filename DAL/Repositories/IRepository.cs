﻿using DAL.DTO;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public interface IRepository<TType, TDTOType>
        where TType : class, IEntity
        where TDTOType : class, IDTO
    {
        ICollection<TDTOType> GetAll();
        ICollection<TDTOType> Find(Func<TType, bool> func);
        TDTOType Get(Expression<Func<TType, bool>> filter);
        TDTOType Add(TDTOType dto);
        void Update(TDTOType entity);
        void Delete(TDTOType dto);
        void Delete(Int32 entityId);
    }
}
