﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public class GoodRepository : GenericRepository<Good, GoodDTO, DatabaseContext>, IGoodRepository
    {
    }
}
