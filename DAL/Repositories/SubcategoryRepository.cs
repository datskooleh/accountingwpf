﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public class SubcategoryRepository : GenericRepository<Subcategory, SubcategoryDTO, DatabaseContext>, ISubcategoryRepository
    {
    }
}
