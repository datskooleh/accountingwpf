﻿using AutoMapper;
using DAL.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public abstract class GenericRepository<TType, TDTOType, TContext> : IRepository<TType, TDTOType>
        where TContext : DatabaseContext
        where TType : class, IEntity
        where TDTOType : class, IDTO
    {
        // ReSharper disable once InconsistentNaming
        //private static readonly TContext context = new TContext();

        private readonly object locker = new object();

        private static readonly TContext context = (TContext)DatabaseContext.Instance;

        public static TContext Context
        {
            get
            {
                return context;
            }
        }

        public TDTOType Add(TDTOType dto)
        {
            TType tmpEntity = Mapper.Map<TDTOType, TType>(dto);
            tmpEntity = Context.Set<TType>().Add(tmpEntity);
            Context.SaveChanges();
            dto = Mapper.Map<TType, TDTOType>(tmpEntity);

            return dto;

        }

        public virtual void Update(TDTOType dto)
        {
            var tmpEntity = Context.Set<TType>().FirstOrDefault(x => x.Id == dto.Id);
            if (tmpEntity == null)
            {
                throw new ArgumentException("No such item exists");
            }

            Mapper.Map<TDTOType, TType>(dto, tmpEntity);
            Context.Entry(tmpEntity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public virtual void Delete(TDTOType dto)
        {
            Delete(dto.Id);
        }

        public virtual void Delete(Int32 entityId)
        {
            var tmpEntity = Context.Set<TType>().FirstOrDefault(x => x.Id == entityId);
            if (tmpEntity == null)
            {
                throw new ArgumentException("No such item exists");
            }

            Context.Entry(tmpEntity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public TDTOType Get(Expression<Func<TType, bool>> filter)
        {
            return Mapper.Map<TType, TDTOType>(Context.Set<TType>().SingleOrDefault(filter));
        }

        public ICollection<TDTOType> GetAll()
        {
            return Mapper.Map<ICollection<TType>, ICollection<TDTOType>>(Context.Set<TType>().ToList());
        }

        public ICollection<TDTOType> Find(Func<TType, bool> func)
        {
            return Mapper.Map<ICollection<TType>, ICollection<TDTOType>>(Context.Set<TType>().Where(func).ToList());
        }
    }
}
