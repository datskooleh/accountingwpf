﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public interface ISubcategoryRepository : IRepository<Subcategory, SubcategoryDTO>
    {
    }
}
