﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public class CategoryRepository : GenericRepository<Category, CategoryDTO, DatabaseContext>, ICategoryRepository
    {
    }
}
