﻿using DAL.DTO;
using DAL.Entities;

namespace DAL.Repositories
{
    public interface ICategoryRepository : IRepository<Category, CategoryDTO>
    {
    }
}
