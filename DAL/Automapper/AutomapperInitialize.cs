﻿using AutoMapper;
using DAL.DTO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Automapper
{
    public class AutomapperInitialize
    {
        public AutomapperInitialize()
        {
            Categories();
            Goods();
        }

        private void Categories()
        {
            Mapper.CreateMap<Category, CategoryDTO>();
            Mapper.CreateMap<CategoryDTO, Category>();

            Mapper.CreateMap<Subcategory, SubcategoryDTO>();
            Mapper.CreateMap<SubcategoryDTO, Subcategory>();
        }

        private void Goods()
        {
            Mapper.CreateMap<Good, GoodDTO>();
            Mapper.CreateMap<GoodDTO, Good>();
        }
    }
}
