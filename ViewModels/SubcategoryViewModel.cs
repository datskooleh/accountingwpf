﻿using DAL.DTO;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using ViewModels.Commands;
using Languages;
using Ninject;

namespace ViewModels
{
    public class SubcategoryViewModel : WorkspaceViewModel
    {
        #region ctor
        [Inject]
        public SubcategoryViewModel(ISubcategoryRepository repository)
        {
            base.DisplayName = Strings.Categories;

            subcategories = new ObservableCollection<SubcategoryDTO>();

            Repository = repository;
        }

        #endregion

        #region Fields
        private IRepository<Subcategory, SubcategoryDTO> Repository { get; set; }

        public string Name { get; set; }

        private SubcategoryDTO selected;
        public SubcategoryDTO Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (SubcategoryChanged != null)
                    if (value != null)
                    {
                        if (Selected != null && Selected.Id != value.Id)
                            SubcategoryChanged(value);
                        SubcategoryChanged(value);
                    }

                selected = value;
            }
        }

        private CategoryDTO SelectedCategory { get; set; }

        private RelayCommand addCommand;
        private RelayCommand deleteCommand;
        private RelayCommand updateCommand;

        #endregion //fields

        #region Methods

        public void OnCategoryCangedEvent(CategoryDTO selectedCategory)
        {
            Collection.Clear();

            SelectedCategory = selectedCategory;

            foreach (var entity in Repository.Find(x => x.Category != null && x.Category.Id.Equals(selectedCategory.Id)))
                Collection.Add(entity);
        }

        public void UpdateSubcategory(GoodDTO good, SubcategoryDTO subcategory)
        {
            subcategory.Goods.Add(good);

            Repository.Update(subcategory);
        }

        #endregion // Methods

        #region Observable

        private readonly ObservableCollection<SubcategoryDTO> subcategories;

        public ObservableCollection<SubcategoryDTO> Collection
        {
            get
            {
                return subcategories;
            }
        }

        #endregion //Observable

        #region Events

        public delegate void ChangeSubcategoryHandler(SubcategoryDTO selectedSubcategory);
        public event ChangeSubcategoryHandler SubcategoryChanged;

        public delegate void SubcategoryAddedHandler(SubcategoryDTO subcategory, CategoryDTO category);
        public event SubcategoryAddedHandler SubcategoryAdded;

        #endregion

        #region Commands

        private bool CanAdd
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Name) && SelectedCategory != null)
                {
                    return Collection.Count(subcategory => subcategory.Name.Equals(Name)) == 0;
                }

                return false;
            }
        }

        private bool CanUpdate
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Name) && SelectedCategory != null)
                {
                    return Collection.Count(subcategory => subcategory.Name.Equals(Name)) == 0;
                }

                return false;
            }
        }

        private bool CanDelete
        {
            get
            {
                if (Selected == null)
                    return false;

                var subcategory = Repository.Get(entity => entity.Id == Selected.Id);
                return subcategory != null;
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(action => Add(), predicate => CanAdd);

                return addCommand;
            }
        }

        public RelayCommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                    updateCommand = new RelayCommand(action => Update(), predicate => CanUpdate);

                return updateCommand;
            }
        }

        public RelayCommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                    deleteCommand = new RelayCommand(param => Delete(), param => CanDelete);

                return deleteCommand;
            }
        }

        #endregion //Commands

        #region Private Helpers

        //ToDo get category by SelectedCategory and add subcategory tto list
        private void Add()
        {
            SubcategoryDTO subcategory = new SubcategoryDTO();
            subcategory.Name = String.Copy(Name);

            //subcategory.Category = SelectedCategory;

            subcategory = Repository.Add(subcategory);
            
            if (SubcategoryAdded != null)
                SubcategoryAdded(subcategory, SelectedCategory);

            Name = String.Empty;

            Collection.Add(subcategory);
        }

        private void Update()
        {
        }

        private void Delete()
        {
            Repository.Delete(Selected);

            SelectedCategory.Subcategories.Remove(SelectedCategory.Subcategories.Single(item => item.Id.Equals(Selected.Id)));
            Collection.Remove(Selected);

            OnPropertyChanged("Subcategory");
        }

        #endregion //Private Helpers
    }
}
