﻿using DAL.DTO;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using ViewModels.Commands;

namespace ViewModels
{
    public class GoodsCatalogViewModel : WorkspaceViewModel
    {
        #region ctor

        public GoodsCatalogViewModel(IGoodRepository repository, ICategoryRepository categoryRepo, ISubcategoryRepository subcategoryRepo)
        {
            base.DisplayName = "Goods catalog";

            MainCategories = new IntersectCategoriesViewModel(categoryRepo, subcategoryRepo);

            MainCategories.Category.CategoryChanged += OnCategoryChange;
            MainCategories.Subcategory.SubcategoryChanged += OnSubcategoryChange;

            AdditionalCategories = new IntersectCategoriesViewModel(categoryRepo, subcategoryRepo);
            GoodAdded += AdditionalCategories.Subcategory.UpdateSubcategory;

            goods = new ObservableCollection<GoodDTO>();

            Repository = repository;
        }

        #endregion //ctor

        #region Fields

        private IRepository<Good, GoodDTO> Repository { get; set; }

        public IntersectCategoriesViewModel MainCategories { get; set; }

        public IntersectCategoriesViewModel AdditionalCategories { get; set; }

        private GoodDTO selected;
        public GoodDTO Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;

                OnPropertyChanged("Selected");
            }
        }

        public SubcategoryDTO SelectedSubcategory { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public decimal PriceBuy { get; set; }

        public decimal PriceSell { get; set; }

        public int Warranty { get; set; }

        private RelayCommand addCommand { get; set; }

        private RelayCommand updateCommand { get; set; }

        private RelayCommand deleteCommand { get; set; }
        #endregion //Fields

        #region Methods

        public void OnCategoryChange(CategoryDTO category)
        {
            Collection.Clear();

            SelectedSubcategory = null;

            foreach (var item in Repository.Find(good => good != null && good.Subcategory != null && good.Subcategory.Category != null && good.Subcategory.Category.Id.Equals(category.Id)))
                Collection.Add(item);
        }

        public void OnSubcategoryChange(SubcategoryDTO subcategory)
        {
            Collection.Clear();

            foreach (var item in Repository.Find(good => good != null && good.Subcategory != null && good.Subcategory.Id.Equals(subcategory.Id)))
                Collection.Add(item);
        }

        #endregion //Methods

        #region Observable

        private readonly ObservableCollection<GoodDTO> goods;

        public ObservableCollection<GoodDTO> Collection
        {
            get
            {
                return goods;
            }
        }

        #endregion //Observable

        #region Events

        public delegate void GoodAddedHandler(GoodDTO good, SubcategoryDTO subcategory);
        public event GoodAddedHandler GoodAdded;

        #endregion //Events

        #region Commands

        private bool CanAdd
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Name) && AdditionalCategories.Subcategory != null && AdditionalCategories.Subcategory.Selected != null
                    && Warranty >= 0)
                {
                    return Collection.Count(category => category.Name.Equals(Name)) == 0;
                }

                return false;
            }
        }

        private bool CanUpdate
        {
            get
            {
                return CanAdd;
            }
        }

        private bool CanDelete
        {
            get
            {
                return true;
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(action => Add(), predicate => CanAdd);

                return addCommand;
            }
        }

        public RelayCommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                    updateCommand = new RelayCommand(action => Update(), predicate => CanUpdate);

                return updateCommand;
            }
        }

        public RelayCommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                    deleteCommand = new RelayCommand(param => Delete(), param => CanDelete);

                return deleteCommand;
            }
        }

        #endregion //Commands

        #region Private Helpers

        //ToDo get category by CategoryId and add subcategory to list
        private void Add()
        {
            GoodDTO good = new GoodDTO()
            {
                Name = String.Copy(Name),
                Count = Count,
                PriceBuy = PriceBuy,
                PriceSell = PriceSell,
                Warranty = Warranty
            };

            Name = String.Empty;
            Count = 0;
            PriceBuy = 0;
            PriceSell = 0;
            Warranty = 0;

            good = Repository.Add(good);

            if (GoodAdded != null)
            {
                GoodAdded(good, AdditionalCategories.Subcategory.Selected);
            }

            if (SelectedSubcategory != null)
                if (good.Subcategory.Id.Equals(SelectedSubcategory.Id))
                    Collection.Add(good);
        }

        private void Update()
        {
        }

        private void Delete()
        {
            Repository.Delete(Selected);

            Collection.Remove(Selected);
        }

        #endregion //Private Helpers

        protected override void OnDispose()
        {
            MainCategories.Category.CategoryChanged -= OnCategoryChange;
            MainCategories.Subcategory.SubcategoryChanged -= OnSubcategoryChange;
            GoodAdded -= MainCategories.Subcategory.UpdateSubcategory;

            base.OnDispose();
        }

    }
}
