﻿using DAL.Repositories;
using Languages;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class IntersectCategoriesViewModel : WorkspaceViewModel
    {
        [Inject]
        public IntersectCategoriesViewModel(ICategoryRepository categoryRepo, ISubcategoryRepository subcategoryRepo)
        {
            this.DisplayName = Strings.Categories;

            Category = new CategoryViewModel(categoryRepo);
            Subcategory = new SubcategoryViewModel(subcategoryRepo);

            Category.CategoryChanged += Subcategory.OnCategoryCangedEvent;
            Subcategory.SubcategoryAdded += Category.UpdateCategory;
        }

        public CategoryViewModel Category { get; private set; }

        public SubcategoryViewModel Subcategory { get; private set; }

        protected override void OnDispose()
        {
            Category.CategoryChanged -= Subcategory.OnCategoryCangedEvent;
            Subcategory.SubcategoryAdded -= Category.UpdateCategory;

            base.OnDispose();
        }
    }
}
