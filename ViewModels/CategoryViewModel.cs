﻿using DAL.DTO;
using DAL.Entities;
using DAL.Repositories;
using Languages;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using ViewModels.Commands;

namespace ViewModels
{
    public class CategoryViewModel : WorkspaceViewModel
    {
        #region ctor
        [Inject]
        public CategoryViewModel(ICategoryRepository repository)
        {
            base.DisplayName = Strings.Categories;

            categories = new ObservableCollection<CategoryDTO>();

            foreach (var entity in repository.GetAll())
                Collection.Add(entity);

            Repository = repository;
        }

        #endregion //ctor

        #region Fields

        private IRepository<Category, CategoryDTO> Repository { get; set; }

        public string Name { get; set; }

        private RelayCommand addCommand;
        private RelayCommand deleteCommand;
        private RelayCommand updateCommand;

        private CategoryDTO selected;
        public CategoryDTO Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (CategoryChanged != null)
                    if (value != null
                        && (Selected == null
                        || Selected.Id != value.Id))
                        CategoryChanged(value);

                selected = value;
            }
        }

        #endregion //Fields

        #region Methods

        public void UpdateCategory(SubcategoryDTO subcategory, CategoryDTO category)
        {
            category.Subcategories.Add(subcategory);

            Repository.Update(category);
        }

        #endregion //Methods

        #region Events

        public delegate void CategoryChangedHandler(CategoryDTO selectedCategory);
        public event CategoryChangedHandler CategoryChanged;

        private void ChangeCategory(int id)
        {
            Debug.Assert(false, id.ToString());
        }

        #endregion //Events

        #region Observable

        private readonly ObservableCollection<CategoryDTO> categories;

        public ObservableCollection<CategoryDTO> Collection
        {
            get
            {
                return categories;
            }
        }

        #endregion //Observable

        #region Commands

        private bool CanAdd
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Name))
                {
                    return Collection.Count(category => category.Name.Equals(Name)) == 0;
                }

                return false;
            }
        }

        private bool CanUpdate
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Name))
                {
                    return Collection.Count(category => category.Name.Equals(Name)) == 0;
                }

                return false;
            }
        }

        private bool CanDelete
        {
            get
            {
                return Selected != null
                    && Selected.Subcategories != null
                    && Selected.Subcategories.Count == 0;
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(action => Add(), predicate => CanAdd);

                return addCommand;
            }
        }

        public RelayCommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                    updateCommand = new RelayCommand(action => Update(), predicate => CanUpdate);

                return updateCommand;
            }
        }

        public RelayCommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                    deleteCommand = new RelayCommand(action => Delete(), predicate => CanDelete);

                return deleteCommand;
            }
        }

        #endregion //Commands

        #region Private Helpers

        private void Add()
        {
            CategoryDTO category = new CategoryDTO();
            category.Name = String.Copy(Name);
            category.Subcategories = new List<SubcategoryDTO>();

            category = Repository.Add(category);

            Name = String.Empty;

            Collection.Add(category);
        }

        private void Update()
        {
        }

        private void Delete()
        {
            Repository.Delete(Selected);

            Collection.Remove(Selected);
        }

        #endregion //Private Helpers
    }
}
