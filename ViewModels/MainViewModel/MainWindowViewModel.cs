﻿using DAL.Automapper;
using DAL.Dependency;
using DAL.Repositories;
using Languages;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ViewModels.Commands;

namespace ViewModels
{
    public class MainWindowViewModel : WorkspaceViewModel
    {
        private readonly IKernel kernel = new StandardKernel();

        public MainWindowViewModel()
        {
            new AutomapperInitialize();
            new DependencyBindings();

            kernel.Load(Assembly.Load("DAL"));
        }

        #region Fields

        ObservableCollection<WorkspaceViewModel> workspaces;

        #endregion //Fields

        #region Workspaces

        /// <summary>
        /// Returns the collection of available workspaces to display.
        /// A 'workspace' is a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (workspaces == null)
                {
                    workspaces = new ObservableCollection<WorkspaceViewModel>();
                    workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return workspaces;
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
            this.Workspaces.Remove(workspace);
        }

        #endregion // Workspaces

        #region Commands
        public RelayCommand OpenClientsCommand
        {
            get
            {
                return new RelayCommand(action => OpenClients());
            }
        }


        public RelayCommand OpenCategoriesCommand
        {
            get
            {
                return new RelayCommand(action => OpenCategories());
            }
        }

        public RelayCommand OpenGoodsCatalogCommand
        {
            get
            {
                return new RelayCommand(action => OpenGoodsCatalog());
            }
        }

        #endregion //Commands

        #region Private Helpers

        private void OpenClients()
        {/*
            ClientsViewModel workspace =
                   this.Workspaces.FirstOrDefault(vm => vm is ClientsViewModel)
                   as ClientsViewModel;

            if (workspace == null)
            {
                workspace = new ClientsViewModel();
                this.Workspaces.Add(workspace);
            }

            this.SetActiveWorkspace(workspace);*/
        }

        private void OpenCategories()
        {
            IntersectCategoriesViewModel workspace =
                this.Workspaces.FirstOrDefault(vm => vm is IntersectCategoriesViewModel)
                as IntersectCategoriesViewModel;

            if (workspace == null)
            {
                workspace = new IntersectCategoriesViewModel(kernel.Get<ICategoryRepository>(), kernel.Get<ISubcategoryRepository>());
                this.Workspaces.Add(workspace);
            }

            this.SetActiveWorkspace(workspace);
        }

        private void OpenGoodsCatalog()
        {
            GoodsCatalogViewModel workspace =
                   this.Workspaces.FirstOrDefault(vm => vm is GoodsCatalogViewModel)
                   as GoodsCatalogViewModel;

            if (workspace == null)
            {
                workspace = new GoodsCatalogViewModel(kernel.Get<IGoodRepository>(),
                    kernel.Get<ICategoryRepository>(),
                    kernel.Get<ISubcategoryRepository>());
                this.Workspaces.Add(workspace);
            }

            this.SetActiveWorkspace(workspace);
        }

        void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }

        #endregion // Private Helpers

        protected override void OnDispose()
        {
            foreach (var item in Workspaces)
                item.Dispose();

            base.OnDispose();
        }
    }
}
